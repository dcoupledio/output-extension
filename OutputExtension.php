<?php namespace Decoupled\Core\Extension\Output;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationExtension;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Bridge\Twig\TwigEngine;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Decoupled\Core\Output\Output;
use Decoupled\Core\Output\OutputHandler;
use Decoupled\Core\Action\ActionQueue;
use Decoupled\Core\Output\TemplateLoader;
use Symfony\Component\Templating\PhpEngine;


class OutputExtension extends ApplicationExtension{

    public function getName()
    {
        return 'output.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$out'] = function( $container ){

            return new Output([
                $container['$twig.engine'],
                $container['$php.engine']
            ]);
        };

        $app['$output.handler'] = function(){

            return new OutputHandler();
        };

        $app['$template.name.parser'] = function(){

            return new TemplateNameParser();
        };

        $app['$template.loader'] = function( $container ){

            return new TemplateLoader( $container['$twig.loader'] );
        };        

        $app['$twig.loader'] = function(){

            return new Twig_Loader_Filesystem();
        };

        $app['$twig'] = function( $container ){

            return new Twig_Environment( $container['$twig.loader'] );    
        };

        $app['$twig.engine'] = function( $container ){

            return new TwigEngine( 
                $container['$twig'], 
                $container['$template.name.parser'] 
            );
        };

        $app['$php.engine'] = function( $container ){

            return new PhpEngine( 
                $container['$template.name.parser'],  
                $container['$template.loader']
            );
        };

        $app->setMethod('output', function( $template = null, $params = [] ) use($app){

            if( ($queue = @func_get_arg(0)) instanceof ActionQueue )
            {
                return $app['$output.handler']->getOutput( $queue );
            }

            $output = $app['$out'];

            if( count(func_get_args()) )
            {
                return $output( $template, $params );
            }

            return $output;
        });
    }
}