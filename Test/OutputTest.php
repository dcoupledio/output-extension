<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Output\OutputExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Output\Output;

class OutputTest extends TestCase{

    public function testCanUseExtension()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new OutputExtension() );

        $app->uses( new ActionExtension() );

        $app['$action.queue']->add($app['$action.factory']->make(function() use ($app){

            return $app->output( '@app/output.html.twig' )->with( 'title', 'foo' );
        }));

        $app['$template.loader']->addPath( dirname(__FILE__), 'app' );

        $output = $app->output( '@app/file.html.twig' )->with( 'title', 'foo' );

        $this->assertContains( 'foo', (string) $output );

        $this->assertEquals(
            (string) $output( '@app/file.html.twig' ),
            (string) $output( '@app/file.php' )
        );

        $output = $app->output( $app['$action.queue'] );

        $this->assertEquals( '@app/output.html.twig', $output->getView() );

        return $app;
    }

    /**
     * @depends testCanUseExtension
     */

    public function testCanGetOutputThroughAppMethod( $app )
    {
        $output = $app->output();

        $this->assertInstanceOf( Output::class, $output );
    }
}